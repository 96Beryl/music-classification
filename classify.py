import argparse
from keras.models import load_model
import librosa
import librosa.display
import numpy as np
import os
import pickle


CLASSES = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
model = load_model('model.h5')

with open('params.pickle', 'rb') as f:
    params = pickle.load(f)
    f.close()

    
def soft_vote(predictions):
    soft = np.zeros(len(CLASSES))
    
    for pred in predictions:
        soft = soft + pred
    soft = soft/predictions.shape[0]
    soft = soft.round(2)
    
    top_fits = np.argsort(soft)[::-1][:2]
    probs = [soft[i] for i in top_fits]
    genres = [CLASSES[i] for i in top_fits]
    return probs, genres
 
 
def classify(path):
    song, sr = librosa.load(path)
    spectr = librosa.feature.melspectrogram(song, sr, n_mels=128, n_fft=2048, hop_length=1024)
    spectr = librosa.power_to_db(spectr, ref=np.max)
    
    slices = [spectr[:, i:i+645] for i in range(0, int(spectr.shape[1]/645)*645, 645)]
    slices = np.stack(slices)
    slices = slices.swapaxes(1, 2)
    
    slices = (slices - params['mean'])/params['std']
    preds = model.predict(slices)
    
    probs, genres = soft_vote(preds)
    return probs, genres


def read_dir_recursive(path):
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(".mp3"):
                probs, genres = classify(os.path.join(root, file))
                print('{}:\n{}, probability: {}\n{}, probability: {}\n'.format(file, genres[0], probs[0], genres[1], probs[1]))

                
def read_dir(path):
    for file in os.listdir(path):
        if file.endswith(".mp3"):
            probs, genres = classify(os.path.join(args.dir, file))
            print('{}:\n{}, probability: {}\n{}, probability: {}\n'.format(file, genres[0], probs[0], genres[1], probs[1]))

            
def main(): 
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', nargs='?', default=os.getcwd())
    parser.add_argument('-r', '--recursive', action='store_true')
    args = parser.parse_args()

    if args.recursive:
        read_dir_recursive(args.dir)
    else:
        read_dir(args.dir)
        
if __name__ == '__main__':
    main()
