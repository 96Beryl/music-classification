from sklearn.model_selection import train_test_split
import sklearn

from keras import optimizers
from keras.models import load_model
from keras.callbacks import ModelCheckpoint

import numpy as np
import pickle
from model import get_model

from matplotlib import pyplot as plt
import seaborn as sns

with open('spectrograms.pickle', 'rb') as f:
    x, y = pickle.load(f)
    f.close()

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.30, random_state=310)

mean_x = np.mean(x_train, axis=0)
std_x = np.std(x_train, axis=0)
x_train = (x_train - mean_x) / std_x
x_test = (x_test - mean_x) / std_x

params = {'mean': mean_x, 'std': std_x}
with open('params.pickle', 'wb') as f:
    f.write(pickle.dumps(params))
    f.close()

REGUL_STEP = 0.001

model = get_model(x_train[0].shape, 10, REGUL_STEP)
optimizer = optimizers.Adam(lr=0.001)

BATCH_SIZE = 64
EPOCHS = 80

filepath = 'model.h5'
checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
callbacks_list = [checkpoint]

model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
history = model.fit(
        x_train, y_train,
        batch_size=BATCH_SIZE,
        epochs=EPOCHS,
        validation_data=(x_test, y_test),
        callbacks=callbacks_list,
        verbose=1)

y_pred = np.argmax(model.predict(x_test), axis=1)
y_true = np.argmax(y_test, axis=1)
cm = sklearn.metrics.confusion_matrix(y_true, y_pred)

CLASSES = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
CLASSES_PRED = [i+'_pred' for i in CLASSES]

conf_mat = sns.heatmap(cm, annot=True, fmt="d", xticklabels=CLASSES_PRED, yticklabels=CLASSES, cmap=plt.cm.Blues)
plt.xticks(rotation=45) 
plt.show()