# Music classification
Music classification with CNNs trained on GTZAN dataset.

## Setup

You need the GTZAN dataset, put the genre directories in the same folder as .py scripts.
To run the classificator, you need to generate spectrograms with generate-clean-data.py script and train the model with train.py.

---

## Running

Run classify.py. If ran without arguments, program will try to find files in current working directory. You can also give path to desired folder as an argument. There's -r flag which will search for files in folder recursively.
Program only supports mp3 files.

