import librosa
import numpy as np
import pickle
import os


def main():
    x = []
    y = []
    CLASSES = ['blues', 'classical', 'country', 'disco', 'hiphop', 'jazz', 'metal', 'pop', 'reggae', 'rock']
    
    for dir in CLASSES:
        print('generating spectrograms for {}'.format(dir))
        for file in os.listdir(dir):
            song, sr = librosa.load(os.path.join(dir, file))
            spectr = librosa.feature.melspectrogram(song, sr, n_mels=128, n_fft=2048, hop_length=1024)
            spectr = librosa.power_to_db(spectr, ref=np.max)
            
            x.append(spectr)
            y.append(dir)
        
    spectr_len = np.min([i.shape[1] for i in x])
    x = [i[:, :spectr_len] for i in x]
    
    def gen_vector(class_):
        index = CLASSES.index(class_)
        v = np.zeros(len(CLASSES))
        v[index] = 1
        return v
    y = [gen_vector(i) for i in y]
    
    x = np.stack(x, axis=0)
    x = np.swapaxes(x, 1, 2)
    y = np.stack(y, axis=0)
    
    with open('spectrograms.pickle', 'wb') as f:
        f.write(pickle.dumps((x, y)))
        f.close()

if __name__ == '__main__':
    main()
