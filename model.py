from keras.layers import Input
from keras.layers.convolutional import Conv1D
from keras.layers.core import Activation, Dense, Dropout, Flatten
from keras.layers.pooling import GlobalAveragePooling1D, GlobalMaxPooling1D, MaxPooling1D
from keras.layers.merge import concatenate
from keras.models import Model
from keras.regularizers import l2

def get_model(input_shape, classes_len, regul_step):
    input_layer = Input(shape=input_shape)
    layer = input_layer
    
    layer = Conv1D(filters=128, kernel_size=4, activation='elu' ,kernel_initializer='glorot_normal', kernel_regularizer=l2(regul_step), bias_regularizer=l2(regul_step))(layer)
    layer = MaxPooling1D(4)(layer)
    
    layer = Conv1D(filters=128, kernel_size=4, activation='elu' ,kernel_initializer='glorot_normal', kernel_regularizer=l2(regul_step), bias_regularizer=l2(regul_step))(layer)
    layer = MaxPooling1D(2)(layer)
    
    layer = Conv1D(filters=256, kernel_size=4, activation='elu' ,kernel_initializer='glorot_normal', kernel_regularizer=l2(regul_step), bias_regularizer=l2(regul_step))(layer)
    avg_pool = GlobalAveragePooling1D()(layer)
    max_pool = GlobalMaxPooling1D()(layer)
    
    layer = concatenate([avg_pool, max_pool])
    
    layer = Dense(units=512, activation='sigmoid', kernel_regularizer=l2(regul_step), bias_regularizer=l2(regul_step))(layer)
    layer = Dropout(rate=0.5)(layer)
    
    layer = Dense(units=classes_len, activation='softmax', kernel_regularizer=l2(regul_step), bias_regularizer=l2(regul_step))(layer)
    
    return Model(inputs=input_layer, outputs=layer)